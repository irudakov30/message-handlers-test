package iru.handlers.core;

import iru.message.Message;

/**
 * Created by irudakov on 01.07.15.
 */
public interface MessageHandler {

    public Message handle(Message message) throws MessageHandleException;
}
