package iru.handlers.core;

/**
 * Created by irudakov on 01.07.15.
 */
public class MessageHandleException extends Exception {

    public MessageHandleException() {
    }

    public MessageHandleException(String message) {
        super(message);
    }

    public MessageHandleException(String message, Throwable cause) {
        super(message, cause);
    }

    public MessageHandleException(Throwable cause) {
        super(cause);
    }
}
