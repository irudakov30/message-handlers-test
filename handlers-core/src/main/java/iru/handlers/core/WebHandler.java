package iru.handlers.core;

import iru.message.Message;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by irudakov on 01.07.15.
 */
@Target({TYPE})
@Retention(RUNTIME)
public @interface WebHandler {
    Class<? extends Message> value();
}
