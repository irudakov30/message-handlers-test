package iru.handlers.core;

import iru.message.Message;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by irudakov on 01.07.15.
 */
public class HandlersContext {

    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    private static boolean isInitialized;

    private static final Map<Class<? extends Message>, MessageHandler> handlers =
            new HashMap<Class<? extends Message>, MessageHandler>();

    public static MessageHandler getHandler(Class<? extends Message> key) {
        ReentrantReadWriteLock.ReadLock rLock = lock.readLock();
        rLock.lock();
        try {
            return handlers.get(key);
        } finally {
            rLock.unlock();
        }
    }

    public static void init() {
        final ReentrantReadWriteLock.ReadLock rLock = lock.readLock();
        rLock.lock();
        try {
            if(isInitialized) return;
        } finally {
            rLock.unlock();
        }

        final ReentrantReadWriteLock.WriteLock wLock = lock.writeLock();

        wLock.lock();
        try {
            if(isInitialized) return;

            init1();
            isInitialized = true;
        } finally {
            wLock.unlock();
        }
    }

    private static void init1() {
        Reflections reflections = new Reflections();
        Set<Class<? extends MessageHandler>> subTypes = reflections.getSubTypesOf(MessageHandler.class);

        handlers.clear();

        for (Class<? extends MessageHandler> each : subTypes) {
            try {
                WebHandler webHandler = each.getDeclaredAnnotation(WebHandler.class);
                if (webHandler == null)
                    throw new IllegalStateException(each.getName() + " doesn't have annotation " + WebHandler.class);

                handlers.put(webHandler.value(), each.newInstance());
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
