package iru.handlers.impl;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import iru.dao.OrderDao;
import iru.handlers.core.MessageHandleException;
import iru.handlers.core.MessageHandler;
import iru.handlers.core.WebHandler;
import iru.message.Message;
import iru.message.Order;
import iru.message.OrderResponse;

/**
 * Created by irudakov on 01.07.15.
 */
@WebHandler(Order.class)
public class OrderMessageHandler implements MessageHandler {

    private final OrderDao orderDao;

    public OrderMessageHandler() {
        final MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost"));
        final MongoDatabase db = mongoClient.getDatabase("iru");
        this.orderDao = new OrderDao(db);
    }

    @Override
    public Message handle(Message message) throws MessageHandleException {
        Order order = (Order) message;

        if(order == null || order.getSum() == null || order.getSum().signum() <= 0) {
            throw new OrderSumException("Wrong order sum");
        }

        if(order.getCustomer() == null) {
            throw new OrderCustomerException("Wrong order customer");
        }

        orderDao.createOrder(order);
        int total = orderDao.getTotalOrders(order.getCustomer());

        return new OrderResponse(total);
    }
}
