package iru.handlers.impl;

import iru.handlers.core.MessageHandleException;

/**
 * Created by irudakov on 01.07.15.
 */
public class OrderSumException extends MessageHandleException {

    public OrderSumException() {
    }

    public OrderSumException(String message) {
        super(message);
    }

    public OrderSumException(String message, Throwable cause) {
        super(message, cause);
    }
}
