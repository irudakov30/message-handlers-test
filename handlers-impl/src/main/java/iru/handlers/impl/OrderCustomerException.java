package iru.handlers.impl;

import iru.handlers.core.MessageHandleException;

/**
 * Created by irudakov on 01.07.15.
 */
public class OrderCustomerException extends MessageHandleException {
    public OrderCustomerException() {
    }

    public OrderCustomerException(String message) {
        super(message);
    }

    public OrderCustomerException(String message, Throwable cause) {
        super(message, cause);
    }
}
