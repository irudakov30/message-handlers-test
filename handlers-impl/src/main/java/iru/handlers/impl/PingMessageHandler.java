package iru.handlers.impl;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import iru.dao.PingDao;
import iru.handlers.core.MessageHandleException;
import iru.handlers.core.MessageHandler;
import iru.handlers.core.WebHandler;
import iru.message.Message;
import iru.message.Ping;

/**
 * Created by irudakov on 01.07.15.
 */
@WebHandler(Ping.class)
public class PingMessageHandler implements MessageHandler {

    private final PingDao pingDao;

    public PingMessageHandler() {
        final MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost"));
        final MongoDatabase db = mongoClient.getDatabase("iru");
        pingDao = new PingDao(db);
    }

    @Override
    public Message handle(Message message) throws MessageHandleException {
        Ping ping = (Ping) message;
        return pingDao.incrementPing(ping, 1);
    }
}
