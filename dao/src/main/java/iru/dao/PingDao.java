package iru.dao;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import iru.message.Ping;
import iru.message.Pong;
import org.bson.Document;

/**
 * Created by irudakov on 01.07.15.
 */
public class PingDao {
    private final MongoCollection<Document> pingCollection;

    public PingDao(final MongoDatabase db) {
        pingCollection = db.getCollection("pings");
    }

    public Pong incrementPing(Ping ping, int increment) {
        Document filter = new Document("customer", ping.getCustomer());
        //Atomic operation
        Document result = pingCollection.findOneAndUpdate(filter, new Document("$inc", new Document("ping", increment)), new FindOneAndUpdateOptions().upsert(true));
        return new Pong(result != null ? (result.getInteger("ping") + increment) : increment);
    }


}
