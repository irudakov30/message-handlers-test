package iru.dao;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import iru.message.Order;
import org.bson.Document;

/**
 * Created by irudakov on 01.07.15.
 */
public class OrderDao {

    private final MongoCollection<Document> orderCollection;

    public OrderDao(final MongoDatabase db) {
        orderCollection = db.getCollection("orders");
    }

    public void createOrder(Order order) {
        Document document = new Document("customer", order.getCustomer()).append("sum", order.getSum().doubleValue());
        orderCollection.insertOne(document);
    }

    public int getTotalOrders(String customer) {
        return (int) orderCollection.count(new Document("customer", customer));
    }
}
