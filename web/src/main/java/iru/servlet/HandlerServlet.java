package iru.servlet;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import iru.handlers.core.HandlersContext;
import iru.handlers.core.MessageHandleException;
import iru.handlers.core.MessageHandler;
import iru.message.ErrorMessage;
import iru.message.Message;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by irudakov on 01.07.15.
 */
@WebServlet(urlPatterns={"/handler"})
public class HandlerServlet extends HttpServlet {

    private static final ObjectMapper mapper = new ObjectMapper();
    static {
        mapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doServe(req, resp);
    }

    private void doServe(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        ServletInputStream servletInputStream = req.getInputStream();

        Message message = mapper.readValue(servletInputStream, Message.class);

        if(message == null) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Message type is not supported");
            return;
        }

        MessageHandler messageHandler = HandlersContext.getHandler(message.getClass());

        if(messageHandler == null) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Message type is not supported");
            return;
        }

        try {
            Message responseMessage = messageHandler.handle(message);
            mapper.writeValue(response.getOutputStream(), responseMessage);
        } catch (MessageHandleException e) {
            mapper.writeValue(response.getOutputStream(), new ErrorMessage(e.getMessage()));
        }

    }
}
