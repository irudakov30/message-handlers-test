package iru.listener;


import iru.handlers.core.HandlersContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

/**
 * Created by irudakov on 01.07.15.
 */
@WebListener
public class ServletWebListener implements javax.servlet.ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        HandlersContext.init();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
