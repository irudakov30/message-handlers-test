package iru.message;

/**
 * Created by irudakov on 01.07.15.
 */
public class Ping extends Message {
    private String customer;

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }
}
