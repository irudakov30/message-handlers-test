package iru.message;

/**
 * Created by irudakov on 01.07.15.
 */
public class OrderResponse extends Message {

    private int totalOrders;

    public OrderResponse(int totalOrders) {
        this.totalOrders = totalOrders;
    }

    public int getTotalOrders() {
        return totalOrders;
    }

    public void setTotalOrders(int totalOrders) {
        this.totalOrders = totalOrders;
    }
}
