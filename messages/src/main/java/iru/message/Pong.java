package iru.message;

/**
 * Created by irudakov on 01.07.15.
 */
public class Pong extends Message {
    private int count;

    public Pong(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
