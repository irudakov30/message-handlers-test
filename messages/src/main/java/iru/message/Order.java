package iru.message;

import java.math.BigDecimal;

/**
 * Created by irudakov on 01.07.15.
 */
public class Order extends Message {

    private BigDecimal sum;
    private String customer;

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Order{" +
                "sum=" + sum +
                ", customer='" + customer + '\'' +
                '}';
    }
}
