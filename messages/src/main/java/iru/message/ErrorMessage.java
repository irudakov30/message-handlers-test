package iru.message;

import iru.message.Message;

/**
 * Created by irudakov on 01.07.15.
 */
public final class ErrorMessage extends Message {
    private final String message;

    public ErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
